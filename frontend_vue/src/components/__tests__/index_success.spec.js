import { mount, flushPromises } from '@vue/test-utils'
import Index from '@/components/Index.vue';
import axios from 'axios'
import ElementPlus from 'element-plus';

const tweets = {data:[
    {
        "id": 1,
        "content": "test content id 1",
        "create_time": "2022-10-15T04:22:11.781283Z",
        "name": "test name 1"
    },
    {
        "id": 2,
        "content": "test content id 2",
        "create_time": "2022-10-15T04:25:41.198243Z",
        "name": "test name 2"
    }
]}

const tweet_created = {
    status:201,
    data:{
        "id": 3,
        "content": "test content id 3",
        "create_time": "2022-10-15T04:35:41.198243Z",
        "name": "test name 3"
    }
}

jest.spyOn(axios, 'get').mockResolvedValue(tweets)
jest.spyOn(axios, 'post').mockResolvedValue(tweet_created)


describe("Test success behaviors", () => {
    let wrapper = mount(Index, {global: {
        plugins: [ElementPlus]
    }})
    


    test('Index Tests Loading Datas', async () => {
        await flushPromises()
        expect(axios.get).toHaveBeenCalledTimes(1)
        expect(wrapper.html()).toContain(tweets.data[0].name)
        expect(wrapper.html()).toContain(tweets.data[0].content)
        expect(wrapper.html()).toContain(tweets.data[0].create_time)
        expect(wrapper.html()).toContain(tweets.data[1].name)
        expect(wrapper.html()).toContain(tweets.data[1].content)
        expect(wrapper.html()).toContain(tweets.data[1].create_time)
        
        const trs = wrapper.findAll('tr')
        expect(trs).toHaveLength(3)
        
      
    })
    
    
    test('Index Tests creating tweet success', async () => {
        
        await flushPromises()
        const inputs = wrapper.findAll('textarea')
        expect(inputs).toHaveLength(2)
        
        await inputs[0].setValue(tweet_created.data.name)
        await inputs[1].setValue(tweet_created.data.content)
        await wrapper.find('button').trigger('click')
        await flushPromises()
        expect(axios.post).toHaveBeenCalledTimes(1)
        const trs = wrapper.findAll('tr')
        expect(trs).toHaveLength(4)
        
      
    })



    // test('Index Tests creating tweet failed with more 50 chars name', async () => {
        
    //     await flushPromises()
    //     expect(axios.get).toHaveBeenCalledTimes(1)
    //     const inputs = wrapper.findAll('textarea')
    //     await inputs[0].setValue("asdfjkjkfskjdlsadcjksnjkdcaldjkbcbhsjadbcjabcdbjascdjasbca")
    //     await inputs[1].setValue(tweet_created.data.content)
    //     await wrapper.find('button').trigger('click')
    //     await flushPromises()

    //     expect(axios.post).toHaveBeenCalledTimes(0)
    //     const trs = wrapper.findAll('tr')
    //     expect(trs).toHaveLength(3)
      
    // })

    // test('Index Tests creating tweet failed with empty content', async () => {
        
    //     await flushPromises()
    //     expect(axios.get).toHaveBeenCalledTimes(1)
    //     const inputs = wrapper.findAll('textarea')
        
    //     await inputs[0].setValue(tweet_created.data.name)
    //     await wrapper.find('button').trigger('click')
    //     await flushPromises()

    //     expect(axios.post).toHaveBeenCalledTimes(0)
    //     const trs = wrapper.findAll('tr')
    //     expect(trs).toHaveLength(3)
      
    // })

    // test('Index Tests creating tweet failed with more 50 chars content', async () => {
        
    //     await flushPromises()
    //     expect(axios.get).toHaveBeenCalledTimes(1)
    //     const inputs = wrapper.findAll('textarea')
    //     await inputs[0].setValue(tweet_created.data.name)
    //     await inputs[1].setValue('asdfjkjkfskjdlsadcjksnjkdcaldjkbcbhsjadbcjabcdbjascdjasbca')
    //     await wrapper.find('button').trigger('click')
    //     await flushPromises()

    //     expect(axios.post).toHaveBeenCalledTimes(0)
    //     const trs = wrapper.findAll('tr')
    //     expect(trs).toHaveLength(3)
      
    // })


})


