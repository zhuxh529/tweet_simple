import { mount, flushPromises } from '@vue/test-utils'
import Index from '@/components/Index.vue';
import axios from 'axios'
import ElementPlus from 'element-plus';

const tweets = {data:[
    {
        "id": 1,
        "content": "test content id 1",
        "create_time": "2022-10-15T04:22:11.781283Z",
        "name": "test name 1"
    },
    {
        "id": 2,
        "content": "test content id 2",
        "create_time": "2022-10-15T04:25:41.198243Z",
        "name": "test name 2"
    }
]}

const tweet_created = {
    status:201,
    data:{
        "id": 3,
        "content": "test content id 3",
        "create_time": "2022-10-15T04:35:41.198243Z",
        "name": "test name 3"
    }
}

jest.spyOn(axios, 'get').mockResolvedValue(tweets)
jest.spyOn(axios, 'post').mockResolvedValue(tweet_created)


describe("Test success behaviors", () => {
    let wrapper = mount(Index, {global: {
        plugins: [ElementPlus]
    }})

    test('Index Tests creating tweet failed with empty name', async () => {
        const jsdomAlert = window.alert; 
        window.alert = () => {};
        await flushPromises()
        expect(axios.get).toHaveBeenCalledTimes(1)
        const inputs = wrapper.findAll('textarea')
        await inputs[0].setValue("")
        await inputs[1].setValue(tweet_created.data.content)
        await wrapper.find('button').trigger('click')
        await flushPromises()
    
        expect(axios.post).toHaveBeenCalledTimes(0)
        const trs = wrapper.findAll('tr')
        expect(trs).toHaveLength(3)
        window.alert = jsdomAlert;
      
    })
    

    test('Index Tests creating tweet failed with more 50 chars name', async () => {
        const jsdomAlert = window.alert; 
        window.alert = () => {};
        await flushPromises()
        const inputs = wrapper.findAll('textarea')
        await inputs[0].setValue("asdfjkjkfskjdlsadcjksnjkdcaldjkbcbhsjadbcjabcdbjascdjasbca")
        await inputs[1].setValue(tweet_created.data.content)
        await wrapper.find('button').trigger('click')
        await flushPromises()

        expect(axios.post).toHaveBeenCalledTimes(0)
        const trs = wrapper.findAll('tr')
        expect(trs).toHaveLength(3)
        window.alert = jsdomAlert;
    })

    test('Index Tests creating tweet failed with empty content', async () => {
        const jsdomAlert = window.alert; 
        window.alert = () => {};
        await flushPromises()
        const inputs = wrapper.findAll('textarea')
        
        await inputs[0].setValue(tweet_created.data.name)
        await inputs[1].setValue("")

        await wrapper.find('button').trigger('click')
        await flushPromises()

        expect(axios.post).toHaveBeenCalledTimes(0)
        const trs = wrapper.findAll('tr')
        expect(trs).toHaveLength(3)
        window.alert = jsdomAlert;
      
    })

    test('Index Tests creating tweet failed with more 50 chars content', async () => {
        const jsdomAlert = window.alert; 
        window.alert = () => {};
        await flushPromises()
        const inputs = wrapper.findAll('textarea')
        await inputs[0].setValue(tweet_created.data.name)
        await inputs[1].setValue('asdfjkjkfskjdlsadcjksnjkdcaldjkbcbhsjadbcjabcdbjascdjasbca')
        await wrapper.find('button').trigger('click')
        await flushPromises()

        expect(axios.post).toHaveBeenCalledTimes(0)
        const trs = wrapper.findAll('tr')
        expect(trs).toHaveLength(3)
        window.alert = jsdomAlert;
      
    })


})


