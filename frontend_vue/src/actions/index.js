import axios from "axios";


export const  get_data=async function(){
    return await axios.get('http://127.0.0.1:8000/api/tweet/tweets/',{
        headers:{
            'content-type': 'application/json'
        }
    })
}

export const  save_data=async function(name,content){
    return await axios.post('http://127.0.0.1:8000/api/tweet/tweets/',
    {
        name:name,
        content:content
    })
}
