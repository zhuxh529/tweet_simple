# Front End: frontend_vue

## Set up && Install
``` npm install ```

This will install all dependencies in package.json

## Run on your web
``` npm run serve ```

## Tests
```npm run test:unit```



# Backend: backend

## Set up && Install
``` pip install -r requirements.txt ```

This will install all dependencies in requirements.txt

## Run on localhost
``` python manage.py runserver```


## Tests
``` python manage.py test```

