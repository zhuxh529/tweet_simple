"""
Created on 10/13/22
@author Xihao
"""
from django.urls import path
from .views import TweetList
urlpatterns = [
    path("tweets/", TweetList.as_view(), name="tweets"),
]