from django.test import TestCase, Client
from django.urls import reverse
from rest_framework.test import APIClient
from rest_framework import status

import json

client = APIClient()


class CreateTweetsTest(TestCase):
    def setUp(self):
        self.valid_payload = {
            "name": "abcd",
            "content": "abcd"
        }
        self.invalid_payload_with_null_name = {
            "name": None,
            "content": "abcd"
        }
        self.invalid_payload_with_empty_name = {
            "name": "",
            "content": "abcd"
        }
        self.invalid_payload_with_50char_longer_name = {
            "name": "sdfhjsalkfjksdhfksdhfksdfshdfkljsadfksdhfklasdhfksdhfkshfkjsdhfkfa",
            "content": "abcd"
        }
        self.invalid_payload_with_null_content = {
            "content": None,
            "name": "abcd"
        }
        self.invalid_payload_with_empty_content = {
            "content": "",
            "name": "abcd"
        }
        self.invalid_payload_with_50char_longer_content = {
            "content": "sdfhjsalkfjksdhfksdhfksdfshdfkljsadfksdhfklasdhfksdhfkshfkjsdhfkfa",
            "name": "abcd"
        }

    def test_create_tweets_success(self):
        response = client.post(
            reverse('tweets'),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_tweets_fail_with_null_name(self):
        response = client.post(
            reverse('tweets'),
            data=json.dumps(self.invalid_payload_with_null_name),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_tweets_fail_with_empty_name(self):
        response = client.post(
            reverse('tweets'),
            data=json.dumps(self.invalid_payload_with_empty_name),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_tweets_fail_with_50char_longer_name(self):
        response = client.post(
            reverse('tweets'),
            data=json.dumps(self.invalid_payload_with_50char_longer_name),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_tweets_fail_with_null_content(self):
        response = client.post(
            reverse('tweets'),
            data=json.dumps(self.invalid_payload_with_null_content),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_tweets_fail_with_empty_content(self):
        response = client.post(
            reverse('tweets'),
            data=json.dumps(self.invalid_payload_with_empty_content),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_tweets_fail_with_50char_longer_content(self):
        response = client.post(
            reverse('tweets'),
            data=json.dumps(self.invalid_payload_with_50char_longer_content),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class GetTweetsTest(TestCase):
    def setUp(self):
        valid_payload1 = {
            "name": "abcd",
            "content": "abcd"
        }
        valid_payload2 = {
            "name": "abcdefg",
            "content": "abcdefg"
        }
        client.post(
            reverse('tweets'),
            data=json.dumps(valid_payload1),
            content_type='application/json'
        )
        client.post(
            reverse('tweets'),
            data=json.dumps(valid_payload2),
            content_type='application/json'
        )

    def test_get_tweets_success(self):
        response = client.get(
            reverse('tweets'),
            content_type='application/json'
        )
        # print(response.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)
