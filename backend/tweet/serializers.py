"""
Created on 10/13/22
@author Xihao
"""
from rest_framework import serializers
from .models import Tweet


class TweetSerializer(serializers.ModelSerializer):
    content = serializers.CharField(required=True)
    name = serializers.CharField(required=True)

    def validate(self, data):
        content = data.get("content", None)
        if content:
            if len(content)==0 or len(content)>50:
                raise serializers.ValidationError("Content should be 1 - 50 characters")
        else:
            raise serializers.ValidationError("missing content")

        name = data.get("name", None)
        if name:
            if len(name)==0 or len(name)>50:
                raise serializers.ValidationError("name should be 1 - 50 characters")
        else:
            raise serializers.ValidationError("missing name")
        return data

    class Meta:
        model = Tweet
        fields = (
            "id", "content", "create_time", "name")
